#include <QtCore/QRegularExpression>
#include <QtCore/QUrl>

#include <QtNetwork/QTcpSocket>

#include "AbstractRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "Utils.h"

#include "ClientConnection.h"

ClientConnection::ClientConnection(QTcpSocket *socket, QObject *parent)
	: QObject(parent)
	, socket_(socket)
	, factory_(std::make_unique<RequestHandlerFactory>(socket_))
{
	enableReadFromSocket();
	connect(socket_, &QAbstractSocket::disconnected, this, &QObject::deleteLater);
}

ClientConnection::~ClientConnection() = default;

void ClientConnection::enableReadFromSocket()
{
	connect(socket_, &QIODevice::readyRead, this, &ClientConnection::readFromSocket);
}

void ClientConnection::handleRequest(const QString &method, const QString &url)
{
	disconnect(socket_, &QIODevice::readyRead, 0, 0);

	AbstractRequestHandler *handler = factory_->requestHandler(QUrl::fromPercentEncoding(url.toUtf8()));
	Q_CHECK_PTR(handler);

	connect(handler, &AbstractRequestHandler::finished, this, &ClientConnection::enableReadFromSocket);
	connect(handler, &AbstractRequestHandler::finished, handler, &QObject::deleteLater);

	if (method == "GET") {
		handler->get();
	} else {
		enableReadFromSocket();
	}
}

void ClientConnection::readFromSocket()
{
	while (socket_->canReadLine()) {
		const QRegularExpression re(R"(^(\w+) (\S+) (HTTP/.*)$)");
		const QRegularExpressionMatch match = re.match(Utils::readLineFromSocket(socket_));

		if (match.hasMatch()) {
			handleRequest(match.captured(1), match.captured(2));
		} else {
			qWarning() << APPLICATION_NAME << "Invalid request";
			socket_->readAll();
		}
	}
}
