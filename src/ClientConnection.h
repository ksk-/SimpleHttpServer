#pragma once

#include <QtCore/QObject>

#include <memory>

class QTcpSocket;

class ClientConnection : public QObject
{
	Q_OBJECT

public:
	explicit ClientConnection(QTcpSocket *socket, QObject *parent = nullptr);
	virtual ~ClientConnection();

private:
	Q_DISABLE_COPY(ClientConnection)

private slots:
	void enableReadFromSocket();
	void handleRequest(const QString &method, const QString &url);
	void readFromSocket();

private:
	QTcpSocket *socket_;
	std::unique_ptr<class RequestHandlerFactory> factory_;
};
