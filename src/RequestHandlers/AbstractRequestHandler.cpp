#include <QtCore/QMap>
#include <QtCore/QRegularExpression>

#include <QtNetwork/QTcpSocket>

#include "Utils.h"

#include "AbstractRequestHandler.h"

constexpr auto HTTP_DELEMITER = "\r\n";

namespace
{
	QTextStream &operator<<(QTextStream &out, const QVariantMap &headers)
	{
		for (const QString &key : headers.keys()) {
			out << key << ": " << headers[key].toByteArray() << HTTP_DELEMITER;
		}

		out << HTTP_DELEMITER;

		return out;
	}
}

void AbstractRequestHandler::get()
{
	finish();
}

AbstractRequestHandler::AbstractRequestHandler(QTcpSocket *socket, QStringList &&params, QObject *parent)
	: QObject(parent)
	, socket_(socket)
	, params_(params)
{
	connect(socket_, &QAbstractSocket::disconnected, this, &QObject::deleteLater);
	readRequestHeaders();
}

void AbstractRequestHandler::sendResponse(Http::StatusCode code, QVariantMap headers, const QByteArray &response)
{
	static const QMap<int, QByteArray> statusCodes = {
		{Http::Ok, "OK"},
		{Http::NotFound, "Not found"},
	};

	headers["Connection"] = requestHeaders_.value("Connection", "close");
	headers["Content-Length"] = response.size();

	QTextStream out(socket_);
	out << "HTTP/1.1" << " " << code << " " << statusCodes.value(code) << HTTP_DELEMITER;
	out << headers;
	out << response;
}

QTcpSocket *AbstractRequestHandler::socket() const
{
	return socket_;
}

QString AbstractRequestHandler::param(int index) const
{
	return (index > 0 && index < params_.size()) ? params_[index] : QString::null;
}

void AbstractRequestHandler::finish()
{
	socket_->waitForBytesWritten();

	if (!hasRequestHeader("Connection", "keep-alive")) {
		socket_->close();
	}

	emit finished();
}

void AbstractRequestHandler::readRequestHeaders()
{
	while (socket_->canReadLine()) {
		const QString line = Utils::readLineFromSocket(socket_, HTTP_DELEMITER);

		if (line.isEmpty()) {
			break;
		} else {
			const QRegularExpression re(R"(^(\S+): (\S+)$)");
			const QRegularExpressionMatch match = re.match(line);

			if (match.hasMatch()) {
				requestHeaders_[match.captured(1)] = match.captured(2);
			}
		}
	}
}

bool AbstractRequestHandler::hasRequestHeader(const QString &header, const QString &value) const
{
	const bool hasHeader = requestHeaders_.contains(header);
	return  hasHeader && (QString::compare(requestHeaders_[header].toString(), value, Qt::CaseInsensitive) == 0);
}
