#pragma once

#include <QtCore/QObject>
#include <QtCore/QVariantMap>

class QTcpSocket;

namespace Http
{
	enum StatusCode {
		Ok = 200,
		NotFound = 404,
	};
}

class AbstractRequestHandler : public QObject
{
	Q_OBJECT

public:
	virtual ~AbstractRequestHandler() = default;

	virtual void get();

signals:
	void finished();

protected:
	explicit AbstractRequestHandler(QTcpSocket *socket, QStringList &&params = {}, QObject *parent = nullptr);

	void sendResponse(Http::StatusCode code, QVariantMap headers, const QByteArray &response);

	QTcpSocket *socket() const;
	QString param(int index) const;

private:
	Q_DISABLE_COPY(AbstractRequestHandler)

	void finish();
	void readRequestHeaders();

	bool hasRequestHeader(const QString &header, const QString &value) const;

private:
	QTcpSocket *socket_;
	QStringList params_;
	QVariantMap requestHeaders_;
};
