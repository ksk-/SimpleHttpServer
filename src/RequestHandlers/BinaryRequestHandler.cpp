#include "BinaryRequestHandler.h"

BinaryRequestHandler::BinaryRequestHandler(QTcpSocket *socket, QStringList &&params, QObject *parent)
	: AbstractRequestHandler(socket, std::move(params), parent)
{
}

void BinaryRequestHandler::get()
{
	const QVariantMap headers = {
		{"Content-Type", "application/octet-stream"},
	};

	sendResponse(Http::Ok, headers, "binary data");

	AbstractRequestHandler::get();
}
