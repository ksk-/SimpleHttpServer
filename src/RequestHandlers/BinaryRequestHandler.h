#pragma once

#include "AbstractRequestHandler.h"

class BinaryRequestHandler : public AbstractRequestHandler
{
	Q_OBJECT

public:
	explicit BinaryRequestHandler(QTcpSocket *socket, QStringList &&params = {}, QObject *parent = nullptr);
	virtual ~BinaryRequestHandler() = default;

	virtual void get() override;

private:
	Q_DISABLE_COPY(BinaryRequestHandler)
};
