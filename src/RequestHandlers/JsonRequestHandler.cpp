#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>

#include "JsonRequestHandler.h"

namespace
{
	QJsonObject makeProperties()
	{
		return {
			{"Id", 1},
			{"Name", "Some name"},
			{"Description", "Some text"},
		};
	}

	QByteArray makeJsonDocument(size_t objectCount)
	{
		QJsonArray objects;

		for (size_t i = 0; i < objectCount; ++i) {
			QJsonObject object;
			object["Type"] = "Object";
			object["ObjectName"] = QString("Object_%1").arg(i);
			object["Properties"] = makeProperties();
			objects.push_back(object);
		}

		return QJsonDocument(objects).toJson();
	}
}


JsonRequestHandler::JsonRequestHandler(QTcpSocket *socket, QStringList &&params, QObject *parent)
	: AbstractRequestHandler(socket, std::move(params), parent)
{
}

void JsonRequestHandler::get()
{
	const QVariantMap headers = {
		{"Content-Type", "text/json"},
	};

	sendResponse(Http::Ok, headers, makeJsonDocument(param(1).toUInt()));

	AbstractRequestHandler::get();
}
