#pragma once

#include "AbstractRequestHandler.h"

class JsonRequestHandler : public AbstractRequestHandler
{
	Q_OBJECT

public:
	explicit JsonRequestHandler(QTcpSocket *socket, QStringList &&params = {}, QObject *parent = nullptr);
	virtual ~JsonRequestHandler() = default;

	virtual void get() override;

private:
	Q_DISABLE_COPY(JsonRequestHandler)
};
