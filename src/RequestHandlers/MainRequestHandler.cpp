#include <QtCore/QDir>

#include "Utils.h"

#include "MainRequestHandler.h"

MainRequestHandler::MainRequestHandler(QTcpSocket *socket, QStringList &&params, QObject *parent)
	: AbstractRequestHandler(socket, std::move(params), parent)
{
}

void MainRequestHandler::get()
{
	const QVariantMap headers = {
		{"Content-Type", "text/html"},
	};

	sendResponse(Http::Ok, headers, Utils::readFile(QDir(STATIC_FILES_DIRECTORY).absoluteFilePath("main.html")));

	AbstractRequestHandler::get();
}
