#pragma once

#include "AbstractRequestHandler.h"

class MainRequestHandler : public AbstractRequestHandler
{
	Q_OBJECT

public:
	explicit MainRequestHandler(QTcpSocket *socket, QStringList &&params = {}, QObject *parent = nullptr);
	virtual ~MainRequestHandler() = default;

	virtual void get() override;

private:
	Q_DISABLE_COPY(MainRequestHandler)
};
