#include <QtCore/QRegularExpression>

#include <functional>
#include <vector>

#include "BinaryRequestHandler.h"
#include "JsonRequestHandler.h"
#include "MainRequestHandler.h"
#include "ResourceNotFoundRequestHandler.h"
#include "XmlRequestHandler.h"

#include "RequestHandlerFactory.h"

#define ROUTE(pattern, handler) { \
	pattern, [](QTcpSocket *socket, QStringList &&params) { \
		return new handler(socket, std::move(params)); \
	}, \
},

using RouteFunction = std::function<AbstractRequestHandler *(QTcpSocket *socket, QStringList &&params)>;

RequestHandlerFactory::RequestHandlerFactory(QTcpSocket *socket)
	: socket_(socket)
{
}

AbstractRequestHandler *RequestHandlerFactory::requestHandler(const QString &url) const
{
	QRegularExpression re;
	QRegularExpressionMatch match;

	static const std::vector<std::pair<QString, RouteFunction>> routes = {
		ROUTE(R"(^/$)", MainRequestHandler)
		ROUTE(R"(^/xml\?root=(\w+)&item=(\w+)$)", XmlRequestHandler)
		ROUTE(R"(^/json\?objects=(\d+)$)", JsonRequestHandler)
		ROUTE(R"(^/binary$)", BinaryRequestHandler)
	};

	const auto it = std::find_if(routes.cbegin(), routes.cend(), [&url , &re, &match](const auto &route) -> bool {
		re.setPattern(route.first);
		match = re.match(url);

		return match.hasMatch();
	});

	return (it == routes.cend())
		   ? new ResourceNotFoundRequestHandler(socket_)
		   : it->second(socket_, match.capturedTexts());
}
