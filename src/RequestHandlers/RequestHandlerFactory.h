#include "AbstractRequestHandler.h"

class RequestHandlerFactory
{
public:
	explicit RequestHandlerFactory(QTcpSocket *socket);
	~RequestHandlerFactory() = default;

	AbstractRequestHandler *requestHandler(const QString &url) const;

private:
	Q_DISABLE_COPY(RequestHandlerFactory)

private:
	QTcpSocket *socket_;
};
