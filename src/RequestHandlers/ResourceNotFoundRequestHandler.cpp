#include <QtNetwork/QTcpSocket>

#include "ResourceNotFoundRequestHandler.h"

ResourceNotFoundRequestHandler::ResourceNotFoundRequestHandler(QTcpSocket *socket, QStringList &&params, QObject *parent)
	: AbstractRequestHandler(socket, std::move(params),parent)
{
}

void ResourceNotFoundRequestHandler::get()
{
	sendNotFoundError();
	AbstractRequestHandler::get();
}

void ResourceNotFoundRequestHandler::sendNotFoundError()
{
	const QVariantMap headers = {
		{"Content-Type", "text/html"},
	};

	const QByteArray response = QString("%1: %2").arg(APPLICATION_NAME).arg("Resource is not found").toUtf8();
	sendResponse(Http::NotFound, headers, response);
}
