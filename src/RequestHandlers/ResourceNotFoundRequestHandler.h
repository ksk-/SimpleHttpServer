#pragma once

#include "AbstractRequestHandler.h"

class ResourceNotFoundRequestHandler : public AbstractRequestHandler
{
	Q_OBJECT

public:
	explicit ResourceNotFoundRequestHandler(QTcpSocket *socket, QStringList &&params = {}, QObject *parent = nullptr);
	virtual ~ResourceNotFoundRequestHandler() = default;

	virtual void get() override;

private:
	Q_DISABLE_COPY(ResourceNotFoundRequestHandler)

	void sendNotFoundError();
};
