#include <QtCore/QXmlStreamWriter>

#include "XmlRequestHandler.h"

namespace
{
	QByteArray makeJsonDocument(const QString rootItem, const QString item)
	{
		QByteArray document;

		QXmlStreamWriter writer(&document);
		writer.setAutoFormatting(true);
		writer.writeStartDocument();
		writer.writeStartElement(rootItem);

		for (int i = 0; i < 3; ++i) {
			writer.writeTextElement(QString("%1_%2").arg(item).arg(i), QString::number(i));
		}

		writer.writeEndElement();
		writer.writeEndDocument();

		return document;
	}
}

XmlRequestHandler::XmlRequestHandler(QTcpSocket *socket, QStringList &&params, QObject *parent)
	: AbstractRequestHandler(socket, std::move(params), parent)
{
}

void XmlRequestHandler::get()
{
	const QVariantMap headers = {
		{"Content-Type", "text/xml"},
	};

	sendResponse(Http::Ok, headers, makeJsonDocument(param(1), param(2)));

	AbstractRequestHandler::get();
}
