#pragma once

#include "AbstractRequestHandler.h"

class XmlRequestHandler : public AbstractRequestHandler
{
	Q_OBJECT

public:
	explicit XmlRequestHandler(QTcpSocket *socket, QStringList &&params = {}, QObject *parent = nullptr);
	virtual ~XmlRequestHandler() = default;

	virtual void get() override;

private:
	Q_DISABLE_COPY(XmlRequestHandler)
};
