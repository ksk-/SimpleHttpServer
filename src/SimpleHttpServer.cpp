#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>

#include "ClientConnection.h"

#include "SimpleHttpServer.h"

SimpleHttpServer::SimpleHttpServer(quint16 port, QObject *parent)
	: QObject(parent)
	, tcpServer_(new QTcpServer(this))
	, port_(port)
{
}

SimpleHttpServer::~SimpleHttpServer()
{
	closeAllConnections();
	stop();
}

bool SimpleHttpServer::isRunning() const
{
	return tcpServer_->isListening();
}

quint16 SimpleHttpServer::port() const
{
	return port_;
}

void SimpleHttpServer::start()
{
	if (tcpServer_->listen(QHostAddress::LocalHost, port_)) {
		qDebug() << "Server started";
	} else {
		qFatal(qPrintable(tcpServer_->errorString()));
	}

	connect(tcpServer_, &QTcpServer::newConnection, this, &SimpleHttpServer::setClientConnection);

	emit started();
}

void SimpleHttpServer::stop()
{
	disconnect(tcpServer_, &QTcpServer::newConnection, this, &SimpleHttpServer::setClientConnection);

	if (isRunning()) {
		tcpServer_->close();
		emit stopped();

		qDebug() << "Server stopped";
	}
}

void SimpleHttpServer::closeAllConnections() const
{
	for (auto *clientSocket : findChildren<QTcpSocket *>()) {
		clientSocket->abort();
		clientSocket->deleteLater();
	}
}

void SimpleHttpServer::setClientConnection()
{
	new ClientConnection(tcpServer_->nextPendingConnection(), this);
}
