#pragma once

#include <QtCore/QObject>

class SimpleHttpServer : public QObject
{
	Q_OBJECT

public:
	explicit SimpleHttpServer(quint16 port, QObject *parent = nullptr);
	virtual ~SimpleHttpServer();

	bool isRunning() const;

	quint16 port() const;

	void closeAllConnections() const;

signals:
	void started();
	void stopped();

public slots:
	void start();
	void stop();

private:
	Q_DISABLE_COPY(SimpleHttpServer)

private slots:
	void setClientConnection();

private:
	class QTcpServer *tcpServer_;
	quint16 port_;
};
