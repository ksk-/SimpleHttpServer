#include <QtCore/QFile>

#include <QtNetwork/QTcpSocket>

#include "Utils.h"

namespace Utils
{
	QString readLineFromSocket(QTcpSocket *socket, const QByteArray &delimiter, int timeout)
	{
		QString result;

		if (socket->canReadLine() || (socket->waitForReadyRead(timeout) && socket->canReadLine())) {
			result = QString::fromUtf8(socket->readLine());

			if (result.endsWith(delimiter)) {
				result.resize(result.size() - delimiter.size());
			}
		}

		return result;
	}

	QByteArray readFile(const QString &path)
	{
		QFile file(path);
		return file.open(QIODevice::ReadOnly) ? file.readAll() : QByteArray();
	}
}
