#pragma once

#include <QtCore/QString>

namespace Utils
{
	QString readLineFromSocket(class QTcpSocket *socket, const QByteArray &delimiter = "\n", int timeout = 30000);
	QByteArray readFile(const QString &path);
}
