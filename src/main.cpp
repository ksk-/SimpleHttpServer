#include <QtCore/QCommandLineParser>
#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QStringList>

#ifdef Q_OS_UNIX
#include <signal.h>
#include <unistd.h>
#endif

#include "SimpleHttpServer.h"

constexpr auto DEFAULT_PORT = 8080;

#ifdef Q_OS_UNIX
void handleTerminate(int signo)
{
	if (signo == SIGTERM) {
		qApp->quit();
	}
}
#endif

int main(int argc, char **argv)
{
#ifdef Q_OS_UNIX
	signal(SIGTERM, &handleTerminate);
#endif

	QCoreApplication app(argc, argv);
	app.setApplicationName(APPLICATION_NAME);

	QCommandLineParser parser;
	parser.setApplicationDescription("Simple http server");
	parser.addHelpOption();

	const QCommandLineOption noDaemonOption({"e", "no-daemon"}, "Run as a regular application.");
	parser.addOption(noDaemonOption);
	parser.process(app);

	SimpleHttpServer server(DEFAULT_PORT);

	QObject::connect(&app, &QCoreApplication::aboutToQuit, &server, &SimpleHttpServer::stop);

	if (!parser.isSet(noDaemonOption)) {
#ifdef Q_OS_UNIX
		daemon(0, 0);
#else
		qWarning() << "Daemonization is not implemented on this platform!";
#endif
	}

	server.start();
	Q_ASSERT(server.isRunning());

	return app.exec();
}
